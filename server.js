import express from 'express';
import axios from 'axios';
import xml2js from 'xml2js';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const parser = new xml2js.Parser();

const app = express();
const port = 3000;

const transformData = (data, id, name) => {
    const results = data.HODN_KAND.map((item) => {
        return {
            id: item['$'].PORADOVE_CISLO,
            count: item['$'].HLASY,
        };
    });
    const partData = data.UCAST[0]['$'];
    const participation = {
        round: partData.KOLO,
        totalDistricts: partData.OKRSKY_CELKEM,
        processedDistricts: partData.OKRSKY_ZPRAC,
        processedDistrictsPercentage: partData.OKRSKY_ZPRAC_PROC,
        registeredVoters: partData.ZAPSANI_VOLICI,
        issuedEnvelopes: partData.VYDANE_OBALKY,
        participationPercentage: partData.UCAST_PROC,
        returnedEnvelopes: partData.ODEVZDANE_OBALKY,
        validVotes: partData.PLATNE_HLASY,
        validVotesPercentage: partData.PLATNE_HLASY_PROC,
    };
    return { id, name, results, participation };
};

const extractRegionData = (data) => data.VYSLEDKY_KRAJ.KRAJ[0];
const findDistrict = (data, id) => extractRegionData(data).OKRES.find((item) => item['$'].NUTS_OKRES === id);
const findMunicipality = (data, id, districtId) =>
    findDistrict(data, districtId).OBEC.find((item) => item['$'].CIS_OBEC === id);

app.get('/api/candidates', async (req, res) => {
    try {
        const response = await axios.get('https://volby.cz/opendata/prez2023nss/xml/perk.xml');
        const xml = response.data;

        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const respData = result.PE_REGKAND.PE_REGKAND_ROW.map((row) => {
                return {
                    id: row.CKAND[0],
                    name: `${row.JMENO[0]} ${row.PRIJMENI[0]}`,
                    occupation: row.POVOLANI[0],
                    age: row.VEK[0],
                };
            });

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/results/regions/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { round } = req.query;
        const response = await axios.get(`https://volby.cz/pls/prez2023nss/vysledky_kraj?kolo=${round}&nuts=${id}`);
        const xml = response.data;

        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const {
                CELKEM,
                ['$']: { NUTS_KRAJ: id, NAZ_KRAJ: name },
            } = extractRegionData(result);
            const respData = transformData(CELKEM[0], id, name);

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/results/regions/:regionId/districts/:id', async (req, res) => {
    try {
        const { regionId, id } = req.params;
        const { round } = req.query;
        const response = await axios.get(
            `https://volby.cz/pls/prez2023nss/vysledky_kraj?kolo=${round}&nuts=${regionId}`,
        );
        const xml = response.data;

        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const {
                CELKEM,
                ['$']: { NUTS_OKRES, NAZ_OKRES },
            } = findDistrict(result, id);
            const respData = transformData(CELKEM[0], NUTS_OKRES, NAZ_OKRES);

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/results/regions/:regionId/districts/:districtId/municipalities/:id', async (req, res) => {
    try {
        const { regionId, districtId, id } = req.params;
        const { round } = req.query;
        const response = await axios.get(
            `https://volby.cz/pls/prez2023nss/vysledky_kraj?kolo=${round}&nuts=${regionId}`,
        );
        const xml = response.data;

        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const municipality = findMunicipality(result, id, districtId);
            const {
                ['$']: { CIS_OBEC, NAZ_OBEC },
            } = municipality;
            const respData = transformData(municipality, CIS_OBEC, NAZ_OBEC);

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/regions', async (req, res) => {
    try {
        const response = await axios.get(`https://volby.cz/opendata/prez2023nss/xml/cnumnuts.xml`);

        const xml = response.data;
        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const respData = result.CNUMNUTS.CNUMNUTS_ROW.filter((item) => item.NUTS?.[0].length === 5).map((row) => {
                return {
                    number: row.NUMNUTS[0],
                    id: row.NUTS[0],
                    name: row.NAZEVNUTS[0],
                };
            });

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/districts', async (req, res) => {
    try {
        const response = await axios.get(`https://volby.cz/opendata/prez2023nss/xml/cnumnuts.xml`);

        const xml = response.data;
        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const respData = result.CNUMNUTS.CNUMNUTS_ROW.filter((item) => item.NUTS?.[0].length === 6).map((row) => {
                return {
                    // 1199 is a special case for Prague, it matches 1100 in the pecoco.xml
                    number: row.NUMNUTS[0] === '1199' ? '1100' : row.NUMNUTS[0],
                    id: row.NUTS[0],
                    name: row.NAZEVNUTS[0],
                    // regionId is the first 5 characters of the district NUTS code
                    regionId: row.NUTS[0].slice(0, 5),
                };
            });

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.get('/api/municipalities', async (req, res) => {
    try {
        const response = await axios.get(`https://volby.cz/opendata/prez2023nss/xml/pecoco.xml`);

        const xml = response.data;
        parser.parseString(xml, (err, result) => {
            if (err) {
                throw err;
            }

            const respData = result.PE_COCO.PE_COCO_ROW.map((row) => {
                return {
                    id2: row.OBEC_PREZ[0],
                    id: row.OBEC[0],
                    name: row.NAZEVOBCE[0],
                    districtNumber: row.OKRES[0],
                    regionNumber: row.KRAJ[0],
                    isSame: row.OBEC_PREZ[0] === row.OBEC[0],
                };
            });

            res.status(200).set('Content-Type', 'application/json');
            res.send({ data: respData });
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching or processing XML');
    }
});

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
});

app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`);
});
