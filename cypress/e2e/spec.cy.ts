describe('template spec', () => {
    it('passes', () => {
        cy.visit('http://localhost:5173');
        cy.get('h1').should('have.text', 'Elections app');
        cy.get('.cy-menu-item-districts').click();
        cy.get('.cy-breadcrumb-level-1').should('have.text', 'Districts');
        cy.get('.cy-menu-item-municipalities').click();
        cy.get('.cy-breadcrumb-level-1').should('have.text', 'Municipalities');
        cy.get('.cy-list-item-500011').click();
        cy.get('.cy-breadcrumb-level-2').should('have.text', '500011');
    });
});
