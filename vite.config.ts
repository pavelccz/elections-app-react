import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src'),
            '@components': path.resolve(__dirname, './src/components'),
            '@config': path.resolve(__dirname, './src/config'),
            '@constants': path.resolve(__dirname, './src/constants'),
            '@context': path.resolve(__dirname, './src/context'),
            '@graphql': path.resolve(__dirname, './src/graphql'),
            '@layouts': path.resolve(__dirname, './src/layouts'),
            '@router': path.resolve(__dirname, './src/router'),
            '@hooks': path.resolve(__dirname, './src/hooks'),
            '@helpers': path.resolve(__dirname, './src/helpers'),
            '@integration': path.resolve(__dirname, './src/integration'),
            '@api': path.resolve(__dirname, './src/api'),
        },
    },
    define: {
        'import.meta.env.PACKAGE_VERSION': JSON.stringify(process.env.npm_package_version),
        global: {},
    },
    server: {
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                changeOrigin: true,
            },
        },
    },
});
