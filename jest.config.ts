/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
        '^@components/(.*)$': '<rootDir>/src/components/$1',
        '^@config/(.*)$': '<rootDir>/src/config/$1',
        '^@constants/(.*)$': '<rootDir>/src/constants/$1',
        '^@context/(.*)$': '<rootDir>/src/context/$1',
        '^@graphql/(.*)$': '<rootDir>/src/graphql/$1',
        '^@layouts/(.*)$': '<rootDir>/src/layouts/$1',
        '^@router/(.*)$': '<rootDir>/src/router/$1',
        '^@hooks/(.*)$': '<rootDir>/src/hooks/$1',
        '^@helpers/(.*)$': '<rootDir>/src/helpers/$1',
        '^@integration/(.*)$': '<rootDir>/src/integration/$1',
        '^@api/(.*)$': '<rootDir>/src/api/$1',
    },
};
