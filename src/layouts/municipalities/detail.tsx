import { useParams } from 'react-router-dom';
import { useFetchMunicipalityResults } from '@hooks/useFetchResults';
import { FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import { getDistrictDataByNumber, getRegionDataByNumber } from '@helpers/helpers';
import Detail from '@components/detail';

const MunicipalitiesDetail: FC = () => {
    const [round, setRound] = useState(1);
    const { id } = useParams();
    const { municipalities, districts, regions } = useContext(Context);
    const municipality = municipalities.find((item) => item.id === id);
    const district = getDistrictDataByNumber(districts, municipality!.districtNumber);
    const region = getRegionDataByNumber(regions, municipality!.regionNumber);
    const { loading, data } = useFetchMunicipalityResults(round, id!, district?.id, district?.regionId);

    const changeRound = (round: number) => setRound(round);

    return (
        <Detail
            data={data}
            title={municipality?.name}
            subTitle={`${district?.name} - ${region?.name}`}
            changeRound={changeRound}
            loading={loading}
            round={round}
        />
    );
};

export default MunicipalitiesDetail;
