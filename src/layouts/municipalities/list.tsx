import { Input, List, Select, Space, Typography } from 'antd';
import { ChangeEvent, FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import { getDistrictDataByNumber, getRegionDataByNumber, normalizeString } from '@helpers/helpers';
import ListComponent from '@components/list';
import ListItem from '@components/list-item';

const { Text } = Typography;

const MunicipalitiesList: FC = () => {
    const { regions, districts, municipalities } = useContext(Context);
    const [search, setSearch] = useState<string>('');
    const [selectedRegion, setSelectedRegion] = useState<string | null>(null);
    const [selectedDistrict, setSelectedDistrict] = useState<string | null>(null);

    const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
        setSearch(normalizeString(e.target.value));
    };

    const onSelectRegion = (value: string) => {
        setSelectedRegion(value);
    };

    const onSelectDistrict = (value: string) => {
        setSelectedDistrict(value);
    };

    const filterOption = (input: string, option?: { label: string; value: string }) =>
        normalizeString(option?.label ?? '').includes(normalizeString(input));

    const regionOptions = regions.map((region) => ({
        value: region.number,
        label: region.name,
    }));

    const districtOptions = districts
        .filter((item) => item.regionId)
        .map((district) => ({
            value: district.number,
            label: district.name,
        }));

    return (
        <ListComponent>
            <Space.Compact style={{ padding: 24, display: 'flex' }}>
                <Select
                    options={regionOptions}
                    onSelect={onSelectRegion}
                    showSearch
                    filterOption={filterOption}
                    allowClear
                    onClear={() => setSelectedRegion(null)}
                    placeholder={'Select region'}
                    style={{ width: 'calc(33.33%)' }}
                />
                <Select
                    options={districtOptions}
                    onSelect={onSelectDistrict}
                    showSearch
                    filterOption={filterOption}
                    allowClear
                    onClear={() => setSelectedDistrict(null)}
                    placeholder={'Select district'}
                    style={{ width: 'calc(33.33%)' }}
                />
                <Input
                    placeholder="Search municipalities"
                    onChange={onSearch}
                    allowClear
                    style={{ width: 'calc(33.33%)' }}
                />
            </Space.Compact>
            <div style={{ overflow: 'auto' }}>
                <List
                    itemLayout="horizontal"
                    dataSource={municipalities.filter(
                        (municipality) =>
                            normalizeString(municipality.name).includes(search) &&
                            (!selectedRegion || municipality.regionNumber === selectedRegion) &&
                            (!selectedDistrict || municipality.districtNumber === selectedDistrict),
                    )}
                    renderItem={(item) => (
                        <ListItem item={item}>
                            <List.Item.Meta
                                title={item.name}
                                description={
                                    getRegionDataByNumber(regions, item.regionNumber)?.name +
                                    ' - ' +
                                    getDistrictDataByNumber(districts, item.districtNumber)?.name
                                }
                            />
                            <Text code>{`{ id: ${item.id} }`}</Text>
                        </ListItem>
                    )}
                />
            </div>
        </ListComponent>
    );
};

export default MunicipalitiesList;
