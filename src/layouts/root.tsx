import { FC, useEffect, useState } from 'react';
import { Outlet, ScrollRestoration, useLocation, useNavigate } from 'react-router-dom';
import MainMenu from '@components/main-menu';
import { Layout } from 'antd';
import Breadcrumbs from '@components/breadcrumbs';
import InfoVersion from '@components/info-version';
import ContentLoader from '@components/content-loader';
import { ROUTES } from '@constants/routes';
import { useFetchInitialData } from '@hooks/useFetchInitialData';

const { Content, Sider } = Layout;

export const Root: FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const { pathname } = useLocation();
    const navigate = useNavigate();
    const { loading } = useFetchInitialData();

    useEffect(() => {
        if (pathname === '/') {
            navigate(ROUTES.MUNICIPALITIES);
        }
    }, [navigate, pathname]);

    return (
        <Layout style={{ minHeight: '100vh', overflow: 'hidden' }}>
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div>
                    <h1 style={{ color: 'white', margin: 16, textAlign: collapsed ? 'center' : 'left' }}>
                        {collapsed ? 'EA' : 'Elections app'}
                    </h1>
                </div>
                <MainMenu />
            </Sider>
            <Layout style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
                <Breadcrumbs />
                <Content style={{ margin: '0 24px 24px 24px', flexGrow: 1, overflow: 'auto' }}>
                    {loading ? <ContentLoader /> : <Outlet />}
                    <ScrollRestoration />
                </Content>
            </Layout>
            <InfoVersion />
        </Layout>
    );
};
