import { Input, List, Typography } from 'antd';
import { ChangeEvent, FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import { normalizeString } from '@helpers/helpers';
import ListComponent from '@components/list';
import ListItem from '@components/list-item';

const { Text } = Typography;

const RegionsList: FC = () => {
    const { regions } = useContext(Context);
    const [search, setSearch] = useState<string>('');

    const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
        setSearch(normalizeString(e.target.value));
    };

    return (
        <ListComponent>
            <div style={{ padding: 24 }}>
                <Input placeholder="Search regions" onChange={onSearch} />
            </div>
            <div style={{ overflow: 'auto' }}>
                <List
                    itemLayout="horizontal"
                    dataSource={regions.filter((region) => normalizeString(region.name).includes(search))}
                    renderItem={(item) => (
                        <ListItem item={item}>
                            <List.Item.Meta title={item.name} />
                            <Text code>{`{ id: ${item.id}, number: ${item.number} }`}</Text>
                        </ListItem>
                    )}
                />
            </div>
        </ListComponent>
    );
};

export default RegionsList;
