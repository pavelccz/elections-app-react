import { useParams } from 'react-router-dom';
import { useFetchRegionResults } from '@hooks/useFetchResults';
import { FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import Detail from '@components/detail';
import { getRegionDataById } from '@helpers/helpers';

const RegionsDetail: FC = () => {
    const [round, setRound] = useState(1);
    const { id } = useParams();
    const { regions } = useContext(Context);
    const region = getRegionDataById(regions, id!);
    const { loading, data } = useFetchRegionResults(round, id!);

    const changeRound = (round: number) => setRound(round);

    return <Detail data={data} title={region?.name} changeRound={changeRound} round={round} loading={loading} />;
};

export default RegionsDetail;
