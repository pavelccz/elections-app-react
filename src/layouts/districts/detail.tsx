import { useParams } from 'react-router-dom';
import { useFetchDistrictResults } from '@hooks/useFetchResults';
import { FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import Detail from '@components/detail';
import { getRegionDataById } from '@helpers/helpers';

const DistrictsDetail: FC = () => {
    const [round, setRound] = useState(1);
    const { id } = useParams();
    const { districts, regions } = useContext(Context);
    const district = districts.find((item) => item.id === id);
    const region = getRegionDataById(regions, district!.regionId);
    const { loading, data } = useFetchDistrictResults(round, id, district?.regionId);

    const changeRound = (round: number) => setRound(round);

    return (
        <Detail
            data={data}
            title={district?.name}
            subTitle={region?.name}
            changeRound={changeRound}
            round={round}
            loading={loading}
        />
    );
};

export default DistrictsDetail;
