import { Input, List, Select, Space, Typography } from 'antd';
import { ChangeEvent, FC, useContext, useState } from 'react';
import { Context } from '@context/context';
import { getRegionDataById, normalizeString } from '@helpers/helpers';
import ListComponent from '@components/list';
import ListItem from '@components/list-item';

const { Text } = Typography;

const DistrictsList: FC = () => {
    const { regions, districts } = useContext(Context);
    const [search, setSearch] = useState<string>('');
    const [selectedRegion, setSelectedRegion] = useState<string | null>(null);

    const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
        setSearch(normalizeString(e.target.value));
    };

    const onSelectRegion = (value: string) => {
        setSelectedRegion(value);
    };

    const filterOption = (input: string, option?: { label: string; value: string }) =>
        normalizeString(option?.label ?? '').includes(normalizeString(input));

    const regionOptions = regions.map((region) => ({
        value: region.id,
        label: region.name,
    }));

    return (
        <ListComponent>
            <Space.Compact style={{ padding: 24 }}>
                <Select
                    options={regionOptions}
                    onSelect={onSelectRegion}
                    showSearch
                    filterOption={filterOption}
                    allowClear
                    onClear={() => setSelectedRegion(null)}
                    placeholder={'Select region'}
                    style={{ width: 'calc(50%)' }}
                />
                <Input placeholder="Search districts" onChange={onSearch} allowClear style={{ width: 'calc(50%)' }} />
            </Space.Compact>
            <div style={{ overflow: 'auto' }}>
                <List
                    itemLayout="horizontal"
                    dataSource={districts.filter(
                        (region) =>
                            normalizeString(region.name).includes(search) &&
                            (!selectedRegion || region.regionId === selectedRegion),
                    )}
                    renderItem={(item) => (
                        <ListItem item={item}>
                            <List.Item.Meta
                                title={item.name}
                                description={getRegionDataById(regions, item.regionId)?.name}
                            />
                            <Text code>{`{ id: ${item.id}, number: ${item.number} }`}</Text>
                        </ListItem>
                    )}
                />
            </div>
        </ListComponent>
    );
};

export default DistrictsList;
