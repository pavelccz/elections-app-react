import axios from 'axios';
import { ResultsForArea } from '@/api';

export const API_V1 = axios.create({
    baseURL: '/api',
});

export enum ApiEndpoint {
    REGIONS = 'regions',
    DISTRICTS = 'districts',
    MUNICIPALITIES = 'municipalities',
    CANDIDATES = 'candidates',
}

export const getRegionApiEndpoint = (id: string) => `results/regions/${id}`;
export const getDistrictApiEndpoint = (regionId: string, id: string) => `results/regions/${regionId}/districts/${id}`;
export const getMunicipalityApiEndpoint = (regionId: string, districtId: string, id: string) =>
    `results/regions/${regionId}/districts/${districtId}/municipalities/${id}`;

export type ResultsForAreaData = {
    data: ResultsForArea;
};
