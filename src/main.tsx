import ReactDOM from 'react-dom/client';
import './index.css';
import ContextProvider from '@context/context';
import { RouterProvider } from 'react-router-dom';
import router from '@router/router';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <ContextProvider>
        <RouterProvider router={router} />
    </ContextProvider>,
);
