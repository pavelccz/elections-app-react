import { District, Region } from '@/api';

export const getDistrictDataByNumber = (districts: District[], number: string) => {
    return districts.find((district) => district.number === number);
};

export const getRegionDataByNumber = (regions: Region[], number: string) => {
    return regions.find((region) => region.number === number);
};

export const getRegionDataById = (regions: Region[], id: string) => {
    return regions.find((region) => region.id === id);
};

export const normalizeString = (str: string) => {
    return str
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '');
};

export const fixTests = () => {
    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation((query) => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // deprecated
            removeListener: jest.fn(), // deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
};
