export const mockParticipationData = {
    issuedEnvelopes: '1000',
    returnedEnvelopes: '800',
    processedDistricts: '50',
    processedDistrictsPercentage: '50',
    totalDistricts: '100',
    registeredVoters: '2000',
    participationPercentage: '40',
    validVotes: '780',
    validVotesPercentage: '97.5',
    round: '1',
};

const mockCandidateResults = [
    { id: '1', count: '100' },
    { id: '2', count: '200' },
    { id: '3', count: '300' },
];

export const mockResultsForArea = {
    id: '1',
    name: 'Area name',
    results: mockCandidateResults,
    participation: mockParticipationData,
};
