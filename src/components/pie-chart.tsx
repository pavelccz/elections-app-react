import { FC, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { blue } from '@ant-design/colors';

interface PieChartData {
    id: string;
    count: string;
    name?: string;
    color?: string;
    age?: string;
    occupation?: string;
}

const ChartContainer = styled.div`
    height: 400px;
    background-color: white;
    border-radius: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 58px;
`;

const ListContainer = styled.div`
    margin-left: 100px;
`;

const CandidateItem = styled.div`
    display: flex;
    align-items: center;
    margin: 16px;
`;

const ColorCircle = styled.div<{ color: string }>`
    width: 20px;
    height: 20px;
    background-color: ${({ color }) => color};
    border-radius: 50%;
    border: 1px solid #ddd; // Slight border for light colors
`;

const Name = styled.div`
    font-weight: bold;
    margin-left: 10px;
`;

const Count = styled.div`
    font-weight: bold;
    text-align: right;
    width: 50px;
    margin-right: 10px;
`;

const PieChart: FC<{ data: PieChartData[] }> = ({ data }) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        if (canvasRef.current) {
            const canvas = canvasRef.current;
            const context = canvas.getContext('2d');

            if (context) {
                const total = data.reduce((acc, candidate) => acc + parseInt(candidate.count, 10), 0);
                let startAngle = 0;

                data.forEach((candidate) => {
                    const sliceAngle = (parseInt(candidate.count, 10) / total) * 2 * Math.PI;
                    context.fillStyle = candidate.color ?? blue.primary!;
                    context.beginPath();
                    context.moveTo(canvas.width / 2, canvas.height / 2);
                    context.arc(
                        canvas.width / 2,
                        canvas.height / 2,
                        canvas.width / 2,
                        startAngle,
                        startAngle + sliceAngle,
                    );
                    context.closePath();
                    context.fill();

                    startAngle += sliceAngle;
                });
            }
        }
    }, [data]);

    return (
        <ChartContainer>
            <canvas ref={canvasRef} width="350" height="350" />
            <ListContainer>
                {data.map((candidate) => (
                    <CandidateItem key={candidate.id}>
                        <Count>{candidate.count}</Count>
                        <ColorCircle color={candidate.color ?? blue.primary!} />
                        <Name>{candidate.name}</Name>
                    </CandidateItem>
                ))}
            </ListContainer>
        </ChartContainer>
    );
};

export default PieChart;
