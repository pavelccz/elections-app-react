import { render, screen } from '@testing-library/react';
import Participation from '@components/participation';
import '@testing-library/jest-dom';
import { fixTests } from '@helpers/helpers.ts';
import { mockParticipationData } from '@helpers/mockData';

fixTests();

describe('Participation Component', () => {
    it('renders without crashing and displays the correct data', () => {
        render(<Participation data={mockParticipationData} />);

        expect(screen.getByText('Issued')).toBeInTheDocument();
        expect(screen.getByText('Returned')).toBeInTheDocument();
        expect(screen.getByText('Processed')).toBeInTheDocument();
        expect(screen.getByText('Processed percentage')).toBeInTheDocument();
        expect(screen.getByText('Total')).toBeInTheDocument();
        expect(screen.getByText('Registered voters')).toBeInTheDocument();
        expect(screen.getByText('Participation percentage')).toBeInTheDocument();
        expect(screen.getByText('Valid votes')).toBeInTheDocument();
        expect(screen.getByText('Valid votes percentage')).toBeInTheDocument();
    });
});
