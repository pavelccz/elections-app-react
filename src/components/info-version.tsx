import { FC, useState } from 'react';

const InfoVersion: FC = () => {
    const [openBox, setOpenBox] = useState<boolean>(false);

    return (
        <div
            onClick={() => setOpenBox(!openBox)}
            style={{
                position: 'fixed',
                bottom: 0,
                right: 20,
                padding: openBox ? '5px 12px' : '2px 7px',
                background: '#dedede',
                border: 'none',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: 'pointer',
                transition: 'all 20ms ease-in-out',
                fontSize: '12px',
                color: '#333333',
            }}
        >
            {!openBox && <div>i</div>}
            {openBox && (
                <div>
                    <div>{`app version: v${import.meta.env.PACKAGE_VERSION}`}</div>
                </div>
            )}
        </div>
    );
};

export default InfoVersion;
