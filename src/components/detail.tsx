import { Radio, Typography } from 'antd';
import Graph from '@components/graph';
import Participation from '@components/participation';
import ContentLoader from '@components/content-loader';
import { FC } from 'react';
import { ResultsForArea } from '@/api';

const { Title, Text } = Typography;

interface RoundOption {
    label: string;
    value: number;
}

interface DetailProps {
    data?: ResultsForArea;
    title?: string;
    subTitle?: string;
    changeRound: (round: number) => void;
    loading: boolean;
    round: number;
}

const roundOptions: RoundOption[] = [
    { label: 'First round', value: 1 },
    { label: 'Second round', value: 2 },
];

const Detail: FC<DetailProps> = ({ data, title, subTitle, changeRound, loading, round }) => {
    return (
        <>
            <Title>{title}</Title>
            <Text type="secondary">{subTitle}</Text>
            <div style={{ marginTop: 16 }}>
                <Radio.Group
                    options={roundOptions}
                    onChange={(e) => changeRound(e.target.value)}
                    value={round}
                    optionType="button"
                    className={'cy-round-selector'}
                />
            </div>

            {data ? (
                <>
                    <Graph data={data.results} />
                    {loading ? <ContentLoader partial /> : <Participation data={data.participation} />}
                </>
            ) : (
                <ContentLoader />
            )}
        </>
    );
};

export default Detail;
