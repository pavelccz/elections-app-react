import { FC, useContext, useState } from 'react';
import { Candidate, CandidateResult } from '@/api';
import { Context } from '@context/context';
import { Segmented } from 'antd';
import { BarChartOutlined, PieChartOutlined } from '@ant-design/icons';
import PieChart from '@components/pie-chart';
import BarChart from '@components/bar-chart';
import { CANDIDATE_COLORS } from '@constants/candidates';
import { reverse, sortBy } from 'lodash';

enum GraphType {
    PieChart = 'pieChart',
    BarChart = 'barChart',
}

interface GraphProps {
    data: CandidateResult[];
}

const graphOptions = [
    { value: GraphType.BarChart, icon: <BarChartOutlined /> },
    { value: GraphType.PieChart, icon: <PieChartOutlined /> },
];

const mergeResults = (results: CandidateResult[], candidates: Candidate[]) =>
    results.map((item) => {
        const additionalDetails = candidates.find((detail) => detail.id === item.id);
        const colors = CANDIDATE_COLORS.find((color) => color.id === item.id);
        return { ...item, ...additionalDetails, ...colors, id: item.id };
    });

const Graph: FC<GraphProps> = ({ data }) => {
    const { candidates } = useContext(Context);
    const [selectedGraph, setSelectedGraph] = useState<GraphType>(GraphType.BarChart);

    const mergedData = reverse(
        sortBy(mergeResults(data, candidates), [
            function (item) {
                return parseInt(item.count, 10);
            },
        ]),
    );

    const onChartChange = (value: GraphType) => setSelectedGraph(value);

    return (
        <>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Segmented options={graphOptions} onChange={onChartChange} />
            </div>
            {selectedGraph === GraphType.BarChart && <BarChart data={mergedData} />}
            {selectedGraph === GraphType.PieChart && <PieChart data={mergedData} />}
        </>
    );
};

export default Graph;
