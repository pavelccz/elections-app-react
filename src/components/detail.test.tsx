import { render, screen } from '@testing-library/react';
import Detail from '@components/detail';
import '@testing-library/jest-dom';
import { mockResultsForArea } from '@helpers/mockData.ts';
import { fixTests } from '@helpers/helpers.ts';

fixTests();

const mockDetailProps = {
    data: mockResultsForArea,
    title: 'Area name',
    subTitle: 'Sub title for area',
    loading: false,
    round: 1,
    changeRound: jest.fn(),
};

describe('Detail Component', () => {
    it('renders without crashing and displays the correct data', () => {
        render(<Detail {...mockDetailProps} />);

        expect(screen.getByText('Area name').tagName).toBe('H1');
        expect(screen.getByText('Sub title for area')).toHaveClass('ant-typography-secondary');
    });
});
