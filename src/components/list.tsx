import { FC, ReactNode } from 'react';
import { theme } from 'antd';

interface ListProps {
    children: ReactNode;
}

const List: FC<ListProps> = ({ children }) => {
    const {
        token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();
    return (
        <div
            style={{
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
                maxWidth: 600,
                width: '100%',
                margin: 'auto',
                overflow: 'hidden',
                display: 'flex',
                flexDirection: 'column',
                maxHeight: '100%',
            }}
        >
            {children}
        </div>
    );
};

export default List;
