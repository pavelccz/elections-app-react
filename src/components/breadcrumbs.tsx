import { Breadcrumb } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import { FC, useEffect, useState } from 'react';
import { ROUTE_NAMES } from '@constants/routes';

const Breadcrumbs: FC = () => {
    const { pathname } = useLocation();
    const navigate = useNavigate();
    const [items, setItems] = useState<{ path?: string; title: string }[]>([]);

    useEffect(() => {
        const path = pathname.split('/');

        setItems(
            path.map((item, index) => {
                if (index === 0) {
                    return {
                        title: 'President elections',
                    };
                }
                return {
                    path: index === path.length - 1 ? undefined : '',
                    title: ROUTE_NAMES[item as keyof typeof ROUTE_NAMES] ?? item,
                    className: `cy-breadcrumb-level-${index}`,
                    onClick:
                        index === path.length - 1
                            ? undefined
                            : () => {
                                  navigate(item);
                              },
                };
            }),
        );
    }, [pathname, navigate]);

    return <Breadcrumb style={{ margin: '16px' }} items={items} />;
};

export default Breadcrumbs;
