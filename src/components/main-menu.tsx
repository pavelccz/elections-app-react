import { Menu, MenuProps } from 'antd';
import { FC, useEffect, useState } from 'react';
import { ROUTE_NAMES, ROUTES } from '@constants/routes';
import { useLocation, useNavigate } from 'react-router-dom';

const items: MenuProps['items'] = [ROUTES.MUNICIPALITIES, ROUTES.DISTRICTS, ROUTES.REGIONS].map((route) => ({
    key: route,
    label: ROUTE_NAMES[route as keyof typeof ROUTE_NAMES] ?? route,
    className: `cy-menu-item-${route}`,
}));

const MainMenu: FC = () => {
    const [current, setCurrent] = useState('');
    const { pathname } = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        const path = pathname.split('/')[1];
        setCurrent(path);
    }, [pathname]);

    const onClick: MenuProps['onClick'] = (e) => {
        navigate(e.key);
    };

    return <Menu theme="dark" items={items} onClick={onClick} selectedKeys={[current]} />;
};

export default MainMenu;
