import { Spin } from 'antd';
import { FC } from 'react';

interface ContentLoaderProps {
    partial?: boolean;
}

const ContentLoader: FC<ContentLoaderProps> = ({ partial = false }) => (
    <div
        style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            boxSizing: 'border-box',
            padding: 48,
            height: partial ? 'auto' : '100%',
        }}
    >
        <Spin size="large" />
    </div>
);

export default ContentLoader;
