import styled from 'styled-components';
import { FC } from 'react';
import { blue } from '@ant-design/colors';

interface BarChartData {
    id: string;
    count: string;
    name?: string;
    color?: string;
    age?: string;
    occupation?: string;
}

interface BarChartProps {
    data: BarChartData[];
}

const ChartContainer = styled.div`
    display: flex;
    align-items: flex-end;
    height: 400px;
    padding: 16px 16px 0 16px;
    background-color: white;
    justify-content: space-around;
    margin-bottom: 58px;
    border-radius: 10px;
`;

const Bar = styled.div.withConfig({
    shouldForwardProp: (prop) => prop !== 'count' && prop !== 'color',
})<{ count: number; color: string }>`
    width: 100%;
    background-color: ${({ color }) => color};
    color: white;
    text-align: center;
    padding: 5px 0;
    border-radius: 5px 5px 0 0;
    height: ${({ count }) => `${count}px`};
    transition: height 0.5s;
`;

const BarHolder = styled.div.withConfig({
    shouldForwardProp: (prop) => prop !== 'wide',
})<{ wide: boolean }>`
    width: ${({ wide }) => (wide ? 160 : 80)}px;
    position: relative;
`;

const Count = styled.div.withConfig({
    shouldForwardProp: (prop) => prop !== 'moveToTop',
})<{ moveToTop: boolean }>`
    text-align: center;
    position: relative;
    top: ${({ moveToTop }) => (moveToTop ? '-30px' : '0')};
    color: ${({ moveToTop }) => (moveToTop ? 'black' : 'inherit')};
    font-weight: bold;
`;

const Label = styled.div`
    text-align: center;
    font-weight: bold;
    margin-top: 8px;
    position: absolute;
    line-height: 1.5;
    width: 100%;
`;

const BarChart: FC<BarChartProps> = ({ data }) => {
    const maxCount = Math.max(...data.map((item) => parseInt(item.count, 10)));

    return (
        <ChartContainer>
            {data.map((item) => {
                const count = (parseInt(item.count, 10) / maxCount) * 300;
                return (
                    <BarHolder key={item.id} wide={data.length < 4}>
                        <Bar count={count} color={item.color ?? blue.primary!}>
                            <Count moveToTop={count < 30}>{item.count}</Count>
                        </Bar>
                        <Label>{item.name}</Label>
                    </BarHolder>
                );
            })}
        </ChartContainer>
    );
};

export default BarChart;
