import { List, theme } from 'antd';
import { useNavigate } from 'react-router-dom';
import { FC, useState } from 'react';

interface ListItemProps {
    item: { id: string };
    children: React.ReactNode;
}

const ListItem: FC<ListItemProps> = ({ item, children }) => {
    const navigate = useNavigate();
    const {
        token: { controlItemBgHover },
    } = theme.useToken();
    const [hoveredItemId, setHoveredItemId] = useState<string | null>(null);

    return (
        <List.Item
            onClick={() => navigate(item.id)}
            style={{
                cursor: 'pointer',
                background: hoveredItemId === item.id ? controlItemBgHover : 'transparent',
                padding: '12px 24px',
                transition: controlItemBgHover,
            }}
            onMouseEnter={() => setHoveredItemId(item.id)}
            onMouseLeave={() => setHoveredItemId(null)}
            className={`cy-list-item-${item.id}`}
        >
            {children}
        </List.Item>
    );
};

export default ListItem;
