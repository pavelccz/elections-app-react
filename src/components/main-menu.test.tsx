import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import MainMenu from '@components/main-menu.tsx';
import { BrowserRouter } from 'react-router-dom';
import { FC, ReactNode } from 'react';
import '@testing-library/jest-dom';

jest.mock('@constants/routes', () => ({
    ROUTE_NAMES: {
        municipalities: 'Municipalities',
        districts: 'Districts',
        regions: 'Regions',
    },
    ROUTES: {
        MUNICIPALITIES: 'municipalities',
        DISTRICTS: 'districts',
        REGIONS: 'regions',
    },
}));

const Wrapper: FC<{ children: ReactNode }> = ({ children }) => <BrowserRouter>{children}</BrowserRouter>;

describe('MainMenu Component', () => {
    it('should render menu items correctly', () => {
        render(<MainMenu />, { wrapper: Wrapper });

        expect(screen.getByText('Municipalities')).toBeInTheDocument();
        expect(screen.getByText('Districts')).toBeInTheDocument();
        expect(screen.getByText('Regions')).toBeInTheDocument();
    });

    it('should navigate to new route on item click', async () => {
        render(<MainMenu />, { wrapper: Wrapper });

        const districts = screen.getByText('Districts');

        await waitFor(async () => {
            await userEvent.click(districts);
            expect(window.location.pathname).toBe('/districts');
        });

        const municipalitiesItem = screen.getByText('Municipalities');

        await waitFor(async () => {
            await userEvent.click(municipalitiesItem);
            expect(window.location.pathname).toBe('/municipalities');
        });
    });
});
