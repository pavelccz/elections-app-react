import { Card, Col, Divider, Row, Statistic } from 'antd';
import { FC } from 'react';
import { ParticipationData } from '@/api';

interface StatisticProps {
    title: string;
    value: string;
    suffix?: string;
    precision?: number;
}

interface ParticipationProps {
    data: ParticipationData;
}

const StatisticCard: FC<StatisticProps> = ({ title, value, suffix = '', precision = 0 }) => (
    <Col span={4}>
        <Card bordered={false}>
            <Statistic
                title={title}
                value={value}
                suffix={suffix}
                groupSeparator=" "
                decimalSeparator=","
                precision={precision}
            />
        </Card>
    </Col>
);

const Participation: FC<ParticipationProps> = ({ data }) => (
    <Row gutter={16}>
        <Divider orientation="left">Envelopes</Divider>
        <StatisticCard title="Issued" value={data.issuedEnvelopes} />
        <StatisticCard title="Returned" value={data.returnedEnvelopes} />

        <Divider orientation="left">Districts</Divider>
        <StatisticCard title="Processed" value={data.processedDistricts} />
        <StatisticCard
            title="Processed percentage"
            value={data.processedDistrictsPercentage}
            suffix="%"
            precision={2}
        />
        <StatisticCard title="Total" value={data.totalDistricts} />

        <Divider orientation="left">Voters</Divider>
        <StatisticCard title="Registered voters" value={data.registeredVoters} />
        <StatisticCard title="Participation percentage" value={data.participationPercentage} suffix="%" precision={2} />
        <StatisticCard title="Valid votes" value={data.validVotes} />
        <StatisticCard title="Valid votes percentage" value={data.validVotesPercentage} suffix="%" precision={2} />
    </Row>
);

export default Participation;
