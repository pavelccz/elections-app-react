export type Candidate = {
    id: string;
    name: string;
    occupation: string;
    age: string;
};

export type Municipality = {
    id: string;
    name: string;
    districtNumber: string;
    regionNumber: string;
    id2: string;
    isSame: boolean;
};

export type District = {
    id: string;
    name: string;
    number: string;
    regionId: string;
};

export type Region = {
    id: string;
    name: string;
    number: string;
};

export type CandidateResult = {
    id: string;
    count: string;
};

export type ParticipationData = {
    round: string;
    totalDistricts: string;
    processedDistricts: string;
    processedDistrictsPercentage: string;
    registeredVoters: string;
    issuedEnvelopes: string;
    participationPercentage: string;
    returnedEnvelopes: string;
    validVotes: string;
    validVotesPercentage: string;
};

export type ResultsForArea = {
    id: string;
    name: string;
    results: CandidateResult[];
    participation: ParticipationData;
};
