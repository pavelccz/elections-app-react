import { useCallback, useEffect, useState } from 'react';
import {
    API_V1,
    getDistrictApiEndpoint,
    getMunicipalityApiEndpoint,
    getRegionApiEndpoint,
    ResultsForAreaData,
} from '@config/api';
import { ResultsForArea } from '@/api';
import { AxiosResponse } from 'axios';

export const useFetchAreaResults = (
    fetchFunction: () => Promise<AxiosResponse<ResultsForAreaData>>,
    round: number = 1,
    condition: boolean = true, // Default condition to always fetch if not provided
) => {
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState<ResultsForArea>();

    const fetchData = useCallback(async () => {
        setLoading(true);
        try {
            const response = await fetchFunction();
            setData(response.data.data);
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
        }
    }, [round, condition]);

    useEffect(() => {
        if (condition) {
            fetchData();
        }
    }, [condition, round, fetchData]);

    return { loading, data };
};

export const useFetchRegionResults = (round: number, id?: string) =>
    useFetchAreaResults(
        () => API_V1.get<ResultsForAreaData>(getRegionApiEndpoint(id!), { params: { round } }),
        round,
        !!id,
    );

export const useFetchDistrictResults = (round: number, id?: string, regionId?: string) =>
    useFetchAreaResults(
        () => API_V1.get<ResultsForAreaData>(getDistrictApiEndpoint(regionId!, id!), { params: { round } }),
        round,
        !!regionId && !!id,
    );

export const useFetchMunicipalityResults = (round: number, id?: string, districtId?: string, regionId?: string) =>
    useFetchAreaResults(
        () =>
            API_V1.get<ResultsForAreaData>(getMunicipalityApiEndpoint(regionId!, districtId!, id!), {
                params: { round },
            }),
        round,
        !!districtId && !!regionId && !!id,
    );
