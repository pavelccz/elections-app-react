import { useCallback, useContext, useEffect, useState } from 'react';
import { API_V1, ApiEndpoint } from '@config/api';
import { Context } from '@context/context';

export const useFetchInitialData = () => {
    const [loading, setLoading] = useState(true);
    const context = useContext(Context);

    const tasks = [
        { endpoint: ApiEndpoint.CANDIDATES, setter: context.setCandidates },
        { endpoint: ApiEndpoint.MUNICIPALITIES, setter: context.setMunicipalities },
        { endpoint: ApiEndpoint.DISTRICTS, setter: context.setDistricts },
        { endpoint: ApiEndpoint.REGIONS, setter: context.setRegions },
    ];

    const fetchData = useCallback(async () => {
        try {
            const fetchPromises = tasks.map((task) =>
                API_V1.get(task.endpoint).then((response) => ({ setter: task.setter, data: response.data.data })),
            );

            const results = await Promise.all(fetchPromises);
            results.forEach(({ setter, data }) => setter(data));
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    return { loading };
};
