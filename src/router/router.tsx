import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom';
import { Root } from '@layouts/root';
import Home from '@layouts/home';
import { ROUTES, SUB_ROUTES } from '@constants/routes';
import RegionsList from '@layouts/regions/list';
import DistrictsList from '@layouts/districts/list';
import MunicipalitiesList from '@layouts/municipalities/list';
import RegionsDetail from '@layouts/regions/detail';
import DistrictsDetail from '@layouts/districts/detail';
import MunicipalitiesDetail from '@layouts/municipalities/detail';

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route element={<Root />} errorElement={<Root />}>
            <Route index element={<Home />} />
            <Route path={ROUTES.REGIONS}>
                <Route index element={<RegionsList />} />
                <Route path={SUB_ROUTES.DETAIL} element={<RegionsDetail />} />
            </Route>
            <Route path={ROUTES.DISTRICTS}>
                <Route index element={<DistrictsList />} />
                <Route path={SUB_ROUTES.DETAIL} element={<DistrictsDetail />} />
            </Route>
            <Route path={ROUTES.MUNICIPALITIES}>
                <Route index element={<MunicipalitiesList />} />
                <Route path={SUB_ROUTES.DETAIL} element={<MunicipalitiesDetail />} />
            </Route>
        </Route>,
    ),
);

export default router;
