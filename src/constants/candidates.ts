import { blue, green, red, gold, purple, cyan, magenta, gray, volcano } from '@ant-design/colors';

export const CANDIDATE_COLORS = [
    { id: '1', color: blue.primary },
    { id: '2', color: green.primary },
    { id: '3', color: red.primary },
    { id: '4', color: gold.primary },
    { id: '5', color: purple.primary },
    { id: '6', color: cyan.primary },
    { id: '7', color: magenta.primary },
    { id: '8', color: gray.primary },
    { id: '9', color: volcano.primary },
];
