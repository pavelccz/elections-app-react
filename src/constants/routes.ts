export const ROUTES = {
    ROOT: '/',
    DISTRICTS: 'districts',
    REGIONS: 'regions',
    MUNICIPALITIES: 'municipalities',
};

export const SUB_ROUTES = {
    DETAIL: ':id',
};

export const ROUTE_NAMES = {
    districts: 'Districts',
    regions: 'Regions',
    municipalities: 'Municipalities',
};
