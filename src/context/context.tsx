import { ReactNode, createContext, FC, useReducer, Reducer } from 'react';
import { Candidate, District, Municipality, Region } from '@/api';

interface ContextState {
    candidates: Candidate[];
    setCandidates: () => void;
    municipalities: Municipality[];
    setMunicipalities: () => void;
    districts: District[];
    setDistricts: () => void;
    regions: Region[];
    setRegions: () => void;
}

const initialState: ContextState = {
    candidates: [],
    setCandidates: () => {}, // placeholder function
    municipalities: [],
    setMunicipalities: () => {}, // placeholder function
    districts: [],
    setDistricts: () => {}, // placeholder function
    regions: [],
    setRegions: () => {}, // placeholder function
};

interface Context {
    candidates: Candidate[];
    setCandidates: (value: Candidate[]) => void;
    municipalities: Municipality[];
    setMunicipalities: (value: Municipality[]) => void;
    districts: District[];
    setDistricts: (value: District[]) => void;
    regions: Region[];
    setRegions: (value: Region[]) => void;
}

export const Context = createContext<Context>(initialState);

const actions = {
    SET_CANDIDATES: 'SET_CANDIDATES',
    SET_MUNICIPALITIES: 'SET_MUNICIPALITIES',
    SET_DISTRICTS: 'SET_DISTRICTS',
    SET_REGIONS: 'SET_REGIONS',
};

type Action = { type: string; value: any };

const reducer: Reducer<ContextState, Action> = (state, action) => {
    switch (action.type) {
        case actions.SET_CANDIDATES:
            return { ...state, candidates: action.value };
        case actions.SET_MUNICIPALITIES:
            return { ...state, municipalities: action.value };
        case actions.SET_DISTRICTS:
            return { ...state, districts: action.value };
        case actions.SET_REGIONS:
            return { ...state, regions: action.value };
        default:
            return state;
    }
};

interface ContextProviderProps {
    children: ReactNode;
}

const ContextProvider: FC<ContextProviderProps> = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const value = {
        candidates: state.candidates,
        setCandidates: (value: Candidate[]) => {
            dispatch({ type: actions.SET_CANDIDATES, value });
        },
        municipalities: state.municipalities,
        setMunicipalities: (value: Municipality[]) => {
            dispatch({ type: actions.SET_MUNICIPALITIES, value });
        },
        districts: state.districts,
        setDistricts: (value: District[]) => {
            dispatch({ type: actions.SET_DISTRICTS, value });
        },
        regions: state.regions,
        setRegions: (value: Region[]) => {
            dispatch({ type: actions.SET_REGIONS, value });
        },
    };

    return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ContextProvider;
