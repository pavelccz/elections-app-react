# Elections app

This is a simple app that fetches data from [volby.cz](https://volby.cz/opendata/prez2023nss/prez2023nss_opendata.htm) and displays it in graphs.

App is built with Vite, React, Express and Ant Design.

## Development & Build Workflow

### Setup

- **Install Dependencies**: Run `yarn install`.

### Development

- **Start Dev Server with Express**: Use `yarn dev` to start the server with live reload including express server fetching data.

### Build & Preview

- **Build ans serve**: Execute `yarn start` to generate a production build in `dist` and serve it with express server.
- **Build**: Execute `yarn build` to generate a production build in `dist`.

### Testing

For testing, we use Jest and React Testing Library. For E2E testing, we use Cypress. To run tests, follow the steps below:

- **Run Tests**: Execute `yarn test` to run tests.
- **Run E2E Tests**: Execute `yarn test:e2e` to run Cypress.

## Resources

This app uses express server that fetches data from [volby.cz](https://volby.cz/opendata/prez2023nss/prez2023nss_opendata.htm) and serves it to the client via Rest Api.

## Ant Design

- [Ant Design](https://ant.design/docs/react/use-with-vite)

## Styling and components

In this app, we use [Ant Design](https://ant.design/docs/react/use-with-vite). Styling is mainly done with inline styles and Ant Design components. Graphs are created with Styled Components (just to show off). 
